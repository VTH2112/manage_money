const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');


router.get("/", async (req, res) => {
    const money = await db.money.find().toArray();
    res.json(money);
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const money = await db.money.find({
        _id: new ObjectId(id),
    })
    res.json(money);
});


router.get('/roomId/:id', async (req, res) => {
    const { id } = req.params;
    const money = await db.money.find({
        roomId: id,
    }).toArray();
    res.status(200).json({ message: 'Tìm kiếm thành công', money: money });
});

router.get('/roomId/:id/month/:month', async (req, res) => {
    const { id, month } = req.params;
    const searchMonth = month.slice(0, 2); // Lấy chuỗi con từ vị trí 0 đến vị trí 2
    const searchYear = month.slice(2);

    console.log(`^${searchMonth}/${searchYear}`, month);
    const money = await db.money.find({
        roomId: id,
        date: { $regex: `^${searchMonth}/${searchYear}`, $options: 'i' },
    }).toArray();

    res.status(200).json({ message: 'Tìm kiếm thành công', money: money });
});


router.post('/addNewMoney', async (req, res) => {
    const { date, money, roomId, tongDien, tongNuoc } = req.body;

    // Kiểm tra xem có bản ghi nào trong database đã đóng tiền cho phòng và tháng tương ứng chưa
    const existingRecord = await db.money.findOne({ roomId: roomId, date: date });
    if (existingRecord) {
        // Nếu đã tồn tại bản ghi, trả về thông báo phòng đã thu tiền tháng này
        return res.status(400).json({ message: 'Phòng đã thu tiền cho tháng này.' });
    }

    // Nếu chưa tồn tại bản ghi, tạo mới bản ghi
    await db.money.insertOne({
        date: date,
        money: money,
        roomId: roomId,
        tongDien: tongDien,
        tongNuoc: tongNuoc
    });

    res.status(200).json({ message: 'Hoàn tất thu tiền' });
});

router.put('/:id', (req, res) => { });

router.delete('/:id', (req, res) => { });

module.exports = router;