const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');


router.get("/getListRoom", async (req, res) => {
    const room = await db.room.find().toArray();
    res.json(room);

});
// get list room by userId
router.get("/getListRoomByUserId/:userId", async (req, res) => {
    const { userId } = req.params;
    const room = await db.room.find({ userId: userId }).toArray();
    res.json(room);

});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const room = await db.room.findOne({
        _id: new ObjectId(id),
    })
    res.json(room);
});



router.post('/addNewCake', async (req, res) => {
    const { cakeName, price, userId } = req.body;

    await db.room.insertOne({
        cakeName: cakeName,
        price: price,
        userId: userId,

    })
    res.status(200).json({ message: 'Tạo bánh thành công' });
});
router.put('/editInfoCake/:id', (req, res) => {
    const { id } = req.params;
    const { cakeName, price, userId } = req.body;
    db.room.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: {
                cakeName: cakeName,
                price: price,
                userId: userId,
            }
        }
    )
    res.status(200).json({ message: 'Cập nhật thành công' });
});

router.delete('/deleteCake/:id', (req, res) => {
    const { id } = req.params;
    db.room.deleteOne(
        { _id: new ObjectId(id) }
    )
    res.status(200).json({ message: `Xóa thành công` });
});

module.exports = router;