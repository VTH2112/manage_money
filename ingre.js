const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');



router.get("/getListIngre", async (req, res) => {
    const ingre = await db.ingre.find().toArray();
    res.json(ingre);
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const ingre = await db.ingre.findOne({
        _id: new ObjectId(id),
    })
    res.json(ingre);
});

router.get('/:ingreId', async (req, res) => {
    const { ingreId } = req.params;
    const ingre = await db.ingre.findOne({
        ingreId: ObjectId(ingreId)
    })
    res.json(ingre);
});

router.post('/addNewIngre', async (req, res) => {
    const { ingreName, price, amount } = req.body;

    await db.ingre.insertOne({
        ingreName: ingreName,
        price: price,
        amount: amount
    })
    res.status(200).json({ message: 'Tạo nguyên liệu thành công' });
});
router.put('/editInfoIngre/:id', (req, res) => {
    const { id } = req.params;
    const { ingreName, price, amount } = req.body;
    db.ingre.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: {
                ingreName: ingreName,
                price: price,
                amount: amount
            }
        }
    )
    res.status(200).json({ message: 'Cập nhật thành công' });
});

router.delete('/deleteIngre/:id', (req, res) => {
    const { id } = req.params;
    db.ingre.deleteOne(
        { _id: new ObjectId(id) }
    )
    res.status(200).json({ message: `Xóa thành công` });
});

module.exports = router;