const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// ...

// Register new member with hashed password
router.post('/register', async (req, res) => {
    const { name, phone, email, password } = req.body;

    // Check if the member already exists
    const existingMem = await db.member.findOne({ email: email });
    const exitsPhone = await db.member.findOne({ phone: phone })
    if (existingMem) {
        return res.status(400).json({ message: 'Email đã tồn tại' });
    }
    if (exitsPhone) {
        return res.status(400).json({ message: 'Số điện thoại đã tồn tại' });
    }
    // check phone if phone == "0856345789" || "0333453010"||"0398176703" set role == admin else set role == user
    let role = 'user';
    if (phone === '0856345789' || phone === '0333453010' || phone === '0398176703') {
        role = 'admin';
    }
    // Hash the password before saving to the database
    const hashedPassword = await bcrypt.hash(password, 10);

    // Save the new member
    await db.member.insertOne({
        name: name,
        phone: phone,
        email: email,
        password: hashedPassword,
        role: role
    });

    res.status(200).json({ message: 'Tạo tài khoản thành công' });
});

// Login user and generate JWT token
router.post('/login', async (req, res) => {
    const { password, phone } = req.body;

    // Check if the user exists
    const user = await db.member.findOne({ phone: phone });

    if (!user) {
        return res.status(400).json({ message: 'Sđt chưa được đăng ký vui lòng thử lại!' });
    }

    // Compare hashed password
    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
        return res.status(400).json({ message: 'Sai mật khẩu vui lòng thử lại' });
    }

    // Generate JWT token
    const token = jwt.sign({ userId: user._id }, 'Vuhai2112', { expiresIn: '1h' });

    res.status(200).json({ message: 'Đăng nhập thành công', token: token , userId:  user._id});
});

// getInforUser

// getInforUser by userId
router.get('/getInforUser/:userId', async (req, res) => {
    const { userId } = req.params;
    const user = await db.member.findOne({
        _id: new ObjectId(userId)
    })
    res.json(user);
});



// ...
module.exports = router;