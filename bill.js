const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');
const { getDb } = require('./db');


router.get("/getListBill", async (req, res) => {
    const bill = await db.bill.find().toArray();
    res.json(bill);
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const bill = await db.bill.findOne({
        _id: new ObjectId(id),
    })
    res.json(bill);
});

router.get('/getBillByUserId/:userId', async (req, res) => {
    const { userId } = req.params;
    const bills = await db.bill.find({ userId: userId }).toArray();
    res.json(bills);
});

router.post('/addNewBill', async (req, res) => {
    const { userId, price, amount, item, phone, address, name } = req.body;

    await db.bill.insertOne({
        // create billName by all info and date format date dd/mm/yyyyThh:mm:ss and region VN
        billName: userId + price + amount + phone + address + name + new Date().toLocaleString('vi-VN', { timeZone: 'Asia/Ho_Chi_Minh' }),
        date: new Date().toLocaleString('vi-VN', { timeZone: 'Asia/Ho_Chi_Minh' }),
        userId: userId,
        price: price,
        amount: amount,
        item: item,
        phone: phone,
        address: address,
        name: name,
        status: 1
    })
    req.app.io.emit('addNewBill');
    res.status(200).json({ message: 'Tạo đơn hàng thành công', status: 200 });
});
router.put('/editStatusBill/:id', (req, res) => {
    const { id } = req.params;
    const { status } = req.body;
    db.bill.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: {
                status: status
            }
        }
    )
    req.app.io.emit('updateStatusBill');
    res.status(200).json({ message: 'Cập nhật trạng thái đơn hàng thành công', status: 200 });
});
router.put('/editInfoBill/:id', (req, res) => {
    const { id } = req.params;
    const { phone, address, name } = req.body;
    db.bill.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: {
                phone: phone,
                address: address,
                name: name
            }
        }
    )
    res.status(200).json({ message: 'Cập nhật đơn hàng thành công', status: 200 });
});

router.delete('/deleteBill/:id/', (req, res) => {
    const { id } = req.params;
    db.bill.deleteOne(
        { _id: new ObjectId(id) }
    )
    req.app.io.emit('billDeleted');
    res.status(200).json({ message: `Xóa đơn hàng thành công` });
});


module.exports = router;