const express = require('express');
const { connectToDb } = require('./db');
const moneyRouter = require('./money');
const roomRouter = require('./room');
const memberRouter = require('./member');
const ingreRouter = require('./ingre');
const productRouter = require('./product');
const billRouter = require('./bill');
const { Storage } = require('@google-cloud/storage');
const jwt = require('jsonwebtoken');
const { createServer } = require('http');
const { Server } = require('socket.io');
const cors = require('cors');

const multer = require('multer');
const path = require('path');
const fs = require('fs');

const app = express();
app.use(cors());
// set port for socket.io



const PORT = process.env.PORT || 8888;
const httpServer = createServer(app);
const io = new Server(httpServer, {
    cors: {
        origin: "*", // Thay đổi "*"" thành domain bạn muốn cho phép kết nối
        methods: ["GET", "POST", "PUT", "DELETE"]
    }
});

app.io = io;
// Middleware to check JWT token
const verifyToken = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, 'Vuhai2112'); // Replace 'YourSecretKey' with your actual secret key
        if (decoded) {
            req.user = decoded; // Store the decoded user information in the request object
            next();
        } else {
            res.status(401).send("Invalid Token");
        }
    } catch (err) {
        res.status(401).send("Invalid Token");
    }
};

// Middleware to exclude memRouter from JWT authentication
const excludeMemRouter = (req, res, next) => {
    if (req.baseUrl === '/member') {
        next();
    } else {
        verifyToken(req, res, next);
    }
};

// Apply middleware
app.use(express.json());
app.use("/money", verifyToken, moneyRouter);
app.use("/room", verifyToken, roomRouter);
app.use("/member", excludeMemRouter, memberRouter); // Exclude memRouter from JWT authentication
app.use("/ingre", verifyToken, ingreRouter);
app.use("/bill", verifyToken, billRouter);
app.use("/product", verifyToken, productRouter);


// Cấu hình nơi lưu trữ và tên file
var destinationPath = './imgs'
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        if (!fs.existsSync(destinationPath)) {
            fs.mkdirSync(destinationPath, { recursive: true });
        }
        cb(null, destinationPath);
    },
    filename: (req, file, cb) => {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        const ext = path.extname(file.originalname);
        cb(null, 'image-' + uniqueSuffix + ext); // Change 'file.fieldname' to 'image'
    },
});

// Tạo middleware upload
const upload = multer({ storage: storage });

// Route để xử lý upload ảnh
app.post('/upload', verifyToken, upload.single('image'), (req, res) => {
    try {
        console.log(req); // Log the entire req object for debugging purposes

        const file = req.file;
        if (!file) {
            res.status(400).json({ message: 'No file uploaded.', status: 400 });
        } else {
            res.status(200).json({ message: 'Upload ảnh thành công', status: 200, fileName: file.filename });
        }
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error', status: 500 });
    }
});

// xóa ảnh
app.delete('/deleteImg/:fileName', verifyToken, (req, res) => {
    try {
        const { fileName } = req.params;
        const filePath = path.join(destinationPath, fileName);
        fs.unlinkSync(filePath);
        res.status(200).json({ message: 'Xóa ảnh thành công', status: 200 });
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error', status: 500 });
    }
});


// Database connection using async/await
async function startServer() {
    try {
        await connectToDb();
        httpServer.listen(PORT, () => {
            console.log(`Server is running on port ${PORT}`);
        });
    } catch (error) {
        console.error('Error connecting to the database:', error);
    }
}

// Start the server
startServer();
