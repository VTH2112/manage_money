const express = require('express');
const { db } = require('./db');
const router = express.Router();
const { ObjectId } = require('mongodb');



router.get("/getListProduct", async (req, res) => {
    const product = await db.product.find().toArray();
    res.json(product);
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    const product = await db.product.findOne({
        _id: new ObjectId(id),
    })
    res.json(product);
});

// find product by cakeId
// GET products by cakeId
router.get('/getByCakeId/:cakeId', async (req, res) => {
    const { cakeId } = req.params;
    const products = await db.product.find({ cakeId: cakeId }).toArray();
    res.json(products);
});

router.post('/addNewProduct', async (req, res) => {
    const { productName, cakeName, amount, price, cakeId, ingreId } = req.body;

    await db.product.insertOne({
        productName: productName,
        price: price,
        amount: amount,
        cakeName: cakeName,
        cakeId: cakeId,
        ingreId: ingreId
    })
    res.status(200).json({ message: 'Tạo nguyên liệu thành công' });
});
router.put('/editInfoProduct/:id', (req, res) => {
    const { id } = req.params;
    const { productName, price, amount } = req.body;
    db.product.updateOne(
        { _id: new ObjectId(id) },
        {
            $set: {
                price: price,
                amount: amount
            }
        }
    )
    res.status(200).json({ message: 'Cập nhật thành công' });
});

router.delete('/deleteProduct/:id/', (req, res) => {
    const { id } = req.params;
    const { productName } = req.params;
    db.product.deleteOne(
        { _id: new ObjectId(id) }
    )
    res.status(200).json({ message: `Xóa thành công` });
});

module.exports = router;